This is a simple example program to interact with HDF5 files for Atom Probe Tomography.


This has been tested on Debian Buster. Compiles with gcc 6.3.0. 

Requires:
===
libhdf5-serial-dev
libhdf5-cpp-103 (or later)
hdf5-helpers
===
