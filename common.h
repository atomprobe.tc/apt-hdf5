#ifndef HDF_EXAMPLE_COMMON_H
#define HDF_EXAMPLE_COMMON_H

#include <hdf5/serial/H5Cpp.h>
#include <hdf5/serial/H5LTpublic.h>

#include <string>
#include <vector>
#include <map>

//Convert a tm time obtained from the gmtime function to UTC.
std::string gmToISO8601UTC(const tm *tmTime);

std::string tmToISO8601Local(const tm *tmTime);

//!Validate an APTH5 file - returns 0 if file is valid
unsigned int validateAPTHDF5(const std::string &filename, bool strict=true, bool verbose=false);

const unsigned int BUF_MAX=1024;



enum
{
	HDF_TYPE_GROUP,
	HDF_TYPE_DATASET,
	HDF_TYPE_LINK,
	HDF_TYPE_USER_DEFINED_LINK,
	HDF_TYPE_ENUM_END
};

//!Return codes for APTH5 reading fucntion
enum
{
	APTH5_ERR_OPEN=1,
	APTH5_ERR_TYPE,
	APTH5_ERR_UNKNOWN_OBJECT,
	APTH5_ERR_EMPTY_OBJECT,
	APTH5_ERR_MISSING_FIELDS,
	APTH5_ERR_MISMATCHED_SIZES,
	APTH5_ERR_UNKNOWN_EXCEPTION,
	APTH5_ERR_NONPHYSICAL_ENTRY

};

const std::map<hid_t,std::string> hdf5TypeMap = { {H5T_INTEGER,"Integer"},
					 {H5T_FLOAT, "Float"},
					 {H5T_BITFIELD, "Bitfield"},
					 {H5T_OPAQUE, "Opaque"},
					 {H5T_COMPOUND, "Compound"},
					 {H5T_REFERENCE, "Reference"},
					 {H5T_ENUM, "Enum"},
					 {H5T_VLEN, "Variable Length"},
					 {H5T_STRING, "String"} 
				};

//!Write string data to a named HDF group
//Note, this creates a nx1 dataset, of type HDF5Dataset with variable size
void writeDataToGroup(H5::H5File &file, 
		const std::string &datasetName, const std::vector<std::string> &dataVec);

//!Write float data to named dataset
void writeDataToGroup(H5::H5File &file, const std::string &datasetName, const std::vector<float> &data,
			bool compression=false);

//!Write Uint64 data to HDF5 file, in a named HDF5Dataset
void writeDataToGroup(H5::H5File &file, const std::string &datasetName, const std::vector<uint64_t> &data,
			bool compression=false);

//!Write a rectangular array of data into an HDF5 file. "data" will be
// passed directly to H5Dwrite, and thus takes on that specified ordering.
void writeDataToGroup(H5::H5File &file, const std::string &datasetName, const float* data, 
			unsigned int rows, unsigned int cols, bool compression=false);


#ifdef DEBUG
bool runCommonTests();
#endif

#endif
