#include "common.h"

#include <string>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <deque>
#include <cassert>
#include <algorithm>
#include <regex>
#include <chrono>
#include <cmath>


//FIXME: Remove me
using namespace H5;
using std::endl;
using std::cerr;
using std::cout;
using std::string;
using std::vector;
using std::deque;
using std::pair;
using std::make_pair;
//!Template function to cast and object to another by the stringstream IO operat
template<class T1, class T2> bool stream_cast(T1 &result, const T2 &obj)
{       
        std::stringstream ss;
        ss << obj; 
        ss >> result; 
        return ss.fail();
}


template<class T2> bool stream_cast(std::string &result, const T2 &obj)
{
        std::stringstream ss; 
        ss << obj; 
        result = ss.str();
        return ss.fail();
}


const char *HDF_TYPE_BYID[] = {"Group",
			"Dataset",
			"Type",
			"Link",
			"User-defined link"};

class HDFQueueEntry
{
	public:
		hid_t parent;
		hid_t offset;
		unsigned int depth;

		HDFQueueEntry(hid_t par, hid_t off, unsigned int dpth) :parent(par),offset(off),depth(dpth)
		{
		}
};

//!Validate that the given dataset names match in size (number of entries).
// File must be open, and dataset paths must exist.
bool validateFieldSizesMatch(H5File &file, 
		const vector<string> &paths, unsigned int &firstMismatch);

//Select a part of a dataset, at a given offset (in size n steps), 
// selecting n elements
bool fetch1DSelection(DataSet &ds, unsigned int offset, 
		unsigned int n, vector<float> &data);

//Fetch rows of data from a given row offset, fetching n rows
bool fetch2DSelection(DataSet &ds, unsigned int offsetRow,
		unsigned int nRows, vector<vector<float> > &data);



// https://stackoverflow.com/questions/26895428/how-do-i-parse-an-iso-8601-date-with-optional-milliseconds-to-a-struct-tm-in-c
// TODO: Replace with C++20 parse when C++20 well supported
bool  parseISO8601(const std::string& input,std::time_t &result)
{
	//Disallow extended format
	if(input.find_first_of(":-") !=std::string::npos)
		return false;

	//Should be 20 chars long
	if (input.length() < 20)
		return false;

	using std::strtol;
	std::tm time = { 0 };
	time.tm_year = strtol(&input[0],nullptr,10) - 1900;
	time.tm_mon = strtol(&input[5],nullptr,10) - 1;
	time.tm_mday = strtol(&input[8],nullptr,10);
	time.tm_hour = strtol(&input[11],nullptr,10);
	time.tm_min = strtol(&input[14],nullptr,10);
	time.tm_sec = strtol(&input[17],nullptr,10);
	time.tm_isdst = 0;

	//Check for optional milliseconds
	const int millis = input.length() > 20 ? strtol(&input[20],nullptr,10) : 0;
	result=timegm(&time) * 1000 + millis;

	return true;
}


//this function will reject strings that are not ISO8601, but may not be strict enough
// does not accept sub-second values. Reduced accuracy dates are not permitted
// Ordinal dates not permitted
bool isISO8601(const std::string &s)
{
	//Too small or too big.
	if(s.size() < 13 || s.size()  >25)
		return false;

	std::string normalised;
	normalised=s;
	
	bool parseAsExtended=false;
	for(auto c : s)
	{
		//Check is a valid identifier
		if(!(isdigit(c) || c == 'T' || c == 'Z' || c == '-' || c == '+' || c == ':'))
			return false;

		if(c == '-' || c == ':')
			parseAsExtended=true;
	}

	//Must contain time/date delimiter
	auto pos = s.find('T');
	if(pos == std::string::npos || pos== s.size()-1)
		return false;


	string date,time;
	date =s.substr(0,pos);
	time = s.substr(pos +1);

	//Validate and convert extended back to regular
	if(parseAsExtended)
	{
		//Validate date and normalise
		if(date.size() > 10 || date.size() < 8)
			return false;

		if(date[4] != '-')
			return false;

		//normalise back to non-extended	
		string tmp;
		for(auto c : date)
		{
			if(c == '-')
				continue;
			tmp+=c;

		}

		date=tmp;

		if(time.size() > 14 || time.size() < 4)
			return false;
		tmp.clear();
		for(auto c : time)
		{
			if(c == ':')
				continue;
			tmp+=c;
		}
		time=tmp;	

	}


	//Parse date
	{
	if(date.size() < 7 || date.size() > 8)
		return false;

	//Check is numerical
	for(auto c : date)
	{
		if(!isdigit(c))
			return false;
	}
	
	//strip year, no need to validate [0000 - 9999] is valid. 
	// we will by "mutual agreement" accept 0000-1582 (see standard which only permits this under some cases)
	string year = date.substr(0,4);
	date=date.substr(4);

	if(date.size() ==3)
	{
		//ordinal date - reject
		//this is permitted by the standard, but we disallow it for simplicity
		return false;
	}

	string month,day;
	month=date.substr(0,2);
	day = date.substr(2);


	int monthInt,dayInt,yearInt;
	stream_cast(monthInt,month);
	stream_cast(dayInt,day);
	stream_cast(yearInt,year);

	if(!monthInt || monthInt > 12)
		return false;

	if(!dayInt || dayInt > 31)
		return false;

	//Validate calendar months to respective day lengths
	switch(monthInt)
	{
		case 2: //feb
		{
			bool isLeap;
			if(yearInt%4)
				isLeap=false;
			else if(!(yearInt%100))
				isLeap=true;
			else
			{
				isLeap=!(yearInt%400);
			}


			if(isLeap)
			{
				if( dayInt > 29)
					return false;
			}
			else
				if(dayInt > 28)
					return false;

			break;
		}
		case 4:
		case 6:
		case 9:
		case 11:
		{
			if(dayInt>30)
				return false;
			break;
		}
		default:
			; //pass
	}	
	}

	//Parse time
	{
		//FIXME: implement me

		//Leading two digits should be time
		unsigned int hrsInt,minsInt;
		stream_cast(hrsInt,time.substr(0,2));
		stream_cast(minsInt,time.substr(2,2));

		if(hrsInt > 24)
			return false;

		if(minsInt > 59)
			return false;



	}

	return true;
}

//Validate that the given datasets make sense,
// return true if OK, false otherwise
bool validatePhysicalParameters(H5::H5File &file)
{
	DataSet ds;
	unsigned int CHUNK_SIZE=1000;

	//Check tip temperature is always positive, and is below the melting point of
	// tungsten
	{
	ds=file.openDataSet("/ExperimentResults/TipTemperature");

	vector<float> data;
	auto nItems=ds.getSpace().getSimpleExtentNpoints();
	auto nFetches = nItems/CHUNK_SIZE+1;

	for(auto ui=0;ui<nFetches;ui++)
	{
		if(!fetch1DSelection(ds,ui,CHUNK_SIZE,data))
		{
			cerr << "Failed to fetch data." << endl;
			return false;
		}

		//check data is OK
		for (auto &v : data)
		{
			const float TUNGSTEN_MELT_KELVIN=3695;
			if( v <0.1 || v >=TUNGSTEN_MELT_KELVIN)
			{
				cerr << "Specimen temperature unphysically hot or cold" << endl;
				return false;
			}
		}
	}
	}


	vector<float> detectorSize;
	//Check detector size is sensible
	{
	ds=file.openDataSet("/ToolStateAndSettings/DetectorSize");
	vector<float> data;
	if(!fetch1DSelection(ds,0,CHUNK_SIZE,data))
	{
		cerr << "Failed to fetch data." << endl;
		return false;
	}

	if(data.size() !=2)
	{
		cerr << "Detector size should have 2 entries (x,y)" << endl;
		return false;
	}
	detectorSize=data;

	//check data is OK - should be > 1 nm in width, < 1km
	// units are m
	for (auto &v : data)
	{
		if( v <1e-9 || v > 1e3 )
			return false;
	}
	}
	
	//check detector type is known
	{
	const vector<string> validTypes = {"DelayLine", "Camera", "WedgeStrip"};

	ds=file.openDataSet("/ToolStateAndSettings/DetectorType");

	string s;
	auto dType=ds.getDataType();
	ds.read(s,dType);

	if(std::find(validTypes.begin(),validTypes.end(),s) == validTypes.end())
		return false;
	}


	//Check flight path spatial and timing are positive
	{
	ds=file.openDataSet("/ToolStateAndSettings/FlightPathSpatial"); 
	vector<float> data;
	if(!fetch1DSelection(ds,0,1,data))
	{
		cerr << "Failed to fetch data." << endl;
		return false;
	}

	if(data.size() !=1)
	{
		cerr << "Should only be a single flight-path entry" << endl;
		return false;
	}

	//check data is OK - should be > 10nm in length, < 1km
	if( data[0] <1e-9 || data[0] > 1e3)
	{
		cerr << "Flight path spatial bounds out of range" << endl;
		return false;
	}
	}

	{
	ds=file.openDataSet("/ToolStateAndSettings/FlightPathTiming"); 
	vector<float> data;
	if(!fetch1DSelection(ds,0,1,data))
	{
		cerr << "Failed to fetch data." << endl;
		return false;
	}

	if(data.size() !=1)
	{
		cerr << "Should only be a single flight-path (timing) entry" << endl;
		return false;
	}

	//check data is OK - should be > 10nm in length, < 1km
	if( data[0] <1e-9 || data[0] > 1e3)
	{
		cerr << "Flight path spatial bounds out of range" << endl;
		return false;
	}
	}

	//Check that laser wavelength within physical limits
	{
	ds=file.openDataSet("/ToolStateAndSettings/LaserWavelength"); 
	vector<float> data;
	fetch1DSelection(ds,0,CHUNK_SIZE,data);

	if(data.size() !=1)
	{
		cerr << "Laser wavelength changed during run" << endl;
		return false;
	}
	}

	//Check Tip2reconSpaceMapping is positive definite, 4x4 matrix

	//Check that Laser energy is always positive
	{
	ds=file.openDataSet("/ExperimentResults/LaserEnergy");

	vector<float> data;
	auto nItems=ds.getSpace().getSimpleExtentNpoints();
	auto nFetches = nItems/CHUNK_SIZE;

	for(auto ui=0;ui<nFetches;ui++)
	{
		if(!fetch1DSelection(ds,ui,CHUNK_SIZE,data))
		{
			cerr << "Failed to fetch data." << endl;
			return false;
		}

		//check data is OK
		for (auto &v : data)
		{
			if( v <0)
			{
				cerr << "Laser energy is negative." << endl;
				return false;
			}
		}
	}
	}

	//Check standing voltage is positive
	{
	ds=file.openDataSet("/ExperimentResults/StandingVoltage");

	vector<float> data;
	auto nItems=ds.getSpace().getSimpleExtentNpoints();
	auto nFetches = nItems/CHUNK_SIZE;

	for(auto ui=0;ui<nFetches;ui++)
	{
		if(!fetch1DSelection(ds,ui,CHUNK_SIZE,data))
		{
			cerr << "Failed to fetch data." << endl;
			return false;
		}

		//check data is OK
		for (auto &v : data)
		{
			if( v <0)
			{
				cerr << "Standing voltage is negative." << endl;
				return false;
			}
		}
	}
	}
	{
	
	}

	//Check reflectron voltage is fixed
	{
	ds=file.openDataSet("/ExperimentResults/ReflectronVoltage");

	vector<float> data;
	auto nItems=ds.getSpace().getSimpleExtentNpoints();
	auto nFetches = nItems/CHUNK_SIZE;

	for(auto ui=0;ui<nFetches;ui++)
	{
		if(!fetch1DSelection(ds,ui,CHUNK_SIZE,data))
		{
			cerr << "Failed to fetch data." << endl;
			return false;
		}

		//check data is OK
		for (auto &v : data)
		{
			if( v <0)
			{
				cerr << "Reflectron voltge is negative." << endl;
				return false;
			}
		}
	}
	}

	//Check that the detector hit positions lie within detector geometry
	{
	ds=file.openDataSet("/ExperimentResults/DetectorHitPositions");

	vector<vector<float>> data;
	auto nItems=ds.getSpace().getSimpleExtentNpoints();
	auto nFetches = nItems/CHUNK_SIZE;

	for(auto ui=0;ui<nFetches;ui++)
	{
		if(!fetch2DSelection(ds,ui,CHUNK_SIZE,data))
		{
			cerr << "Failed to fetch data." << endl;
			return false;
		}

		//check data is OK
		for (auto uj=0u;uj<data.size();uj++)
		{
			for(auto uk=0u;uk<2;uk++)
			{
				//Should lie within [-D/2 , +D/2]
				if( abs(data[uj][uk]) > detectorSize[uk]/2) 
				{
					cerr << "Hit" << uj << " outside detector, on axis:" << uk << endl;
					return false;
				}
			}
		}
	}
	}


	return true;
}

//validate the dataset metadata. Assumes all fields have been validated as present already
bool validateMetaData(H5::H5File &file)
{

	const vector<string> timeFields = { 
			"/ToolEnvironment/ExperimentStartDateUTC", 
			"/ToolEnvironment/ExperimentEndDateUTC", 
			"/ToolEnvironment/ExperimentStartDateLocal"};

	bool valid=true;

	{
	//Check that the time values are actually in ISO8601 format.
	for(auto ui=0u;ui<timeFields.size(); ui++)
	{
		std::string s = timeFields[ui];
		DataSet ds=file.openDataSet(s.c_str());
		
		//get contents
		string text;
		auto dType=ds.getDataType();
		ds.read(text,dType);


		//Check match
		valid = isISO8601(text);

		if(!valid)
			cerr << s<< " does not appear to be ISO8601, found : " << text  << endl;

	}
	}

	//Check the version field makes sense
	{
	const char *VERSION_FIELD="/ExperimentContext/Version";

	DataSet ds=file.openDataSet(VERSION_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);


	//Should be YYYY-MM[.X], 
	// year, month, increment.
	bool versionValid=true;
	if(!(text.size()==7 || text.size()==9))
		versionValid=false;

	for(auto ui=0;ui<4;ui++)
	{
		if(!isdigit(text[ui]))
			versionValid=false;
	}
	if(text[4] != '-')
		versionValid=false;
	for(auto ui=5;ui<7;ui++)
	{
		if(!isdigit(text[ui]))
			versionValid=false;
	}
	
	for(auto ui=5;ui<7;ui++)
	{
		if(!isdigit(text[ui]))
			versionValid=false;
	}
	if(text.size() ==9)
	{
		if(text[7] !='.' || !isdigit(text[8]))
			versionValid=false;
	}

	if(!versionValid)
		cerr << "Version data (" << VERSION_FIELD <<") is not valid" << endl;

	valid= (valid && versionValid);	
	}


	//Check the sample name is short
	{
	const char *SAMPLENAME_FIELD="/ExperimentContext/SampleName";

	DataSet ds=file.openDataSet(SAMPLENAME_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);
	if(text.size() > 200)
	{
		cerr << "Sample name is too long, max 200 chars" << endl;
		valid=false;
	}
	}

	//Check sample identifier size	
	{
	const char *SAMPLEID_FIELD="/ExperimentContext/SampleUniqueIdentifier";

	DataSet ds=file.openDataSet(SAMPLEID_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);
	if(text.size() > 100)
	{
		cerr << "Sample unique ID is too long, max 100 chars" << endl;
		valid=false;
	}
	}


	//Check aperture max identifier size
	{
	const char *APERTUREID_FIELD="/ExperimentContext/ApertureUniqueIdentifier";

	DataSet ds=file.openDataSet(APERTUREID_FIELD);
	auto dType = ds.getDataType();
	string text;
	ds.read(text,dType);
	if(text.size() > 100)
	{
		cerr << "Aperture unique ID is too long, max 100 chars" << endl;
		valid=false;
	}
	}


	return valid;
}

//From https://stackoverflow.com/questions/32424125/c-code-to-get-local-time-offset-in-minutes-relative-to-utc
// return difference in **seconds** of the tm_mday, tm_hour, tm_min, tm_sec members to UTC.
long tz_offset_second(time_t t) {
	struct tm local = *localtime(&t);
	struct tm utc = *gmtime(&t);
	long diff = ((local.tm_hour - utc.tm_hour) * 60 + (local.tm_min - utc.tm_min))
			* 60L + (local.tm_sec - utc.tm_sec);
	int delta_day = local.tm_mday - utc.tm_mday;
	// If |delta_day| > 1, then end-of-month wrap
	if ((delta_day == 1) || (delta_day < -1)) {
		diff += 24L * 60 * 60;
	} else if ((delta_day == -1) || (delta_day > 1)) {
		diff -= 24L * 60 * 60;
	}
	return diff;
}

//Convert a tm time obtained from the gmtime function to UTC.
std::string gmToISO8601UTC(const tm *tmTime)
{
	std::ostringstream ss;
	ss << std::put_time(tmTime,"%FT%TZ"); // convert to string using std::put_time
	return ss.str();
}

std::string tmToISO8601Local(const tm *tmTime)
{
	std::string timeStr;
	{
	std::ostringstream ss;
	ss << std::put_time(tmTime,"%FT%T"); // convert to string using std::put_time
	timeStr=ss.str();
	}

	time_t now = time(NULL);
	long int tmOff = tz_offset_second(now);

	long int hrsOff = tmOff/3600;
	long int minsOff = (tmOff/60) %60;

	//Add hrs offset in 2 digit form, eg +XX
	{
	std::ostringstream ss;
	ss << std::setfill('0') << std::setw(2) << hrsOff;
	if(hrsOff > 0)
		timeStr+="+";
	timeStr+=ss.str();
	}

	//add minutes offset in 2-digit form, eg XX
	{
	std::ostringstream ss;
	timeStr+=":";
	ss<< std::setfill('0') << std::setw(2) << minsOff;
	timeStr+=ss.str();
	}

	return timeStr;
}

bool fetch1DSelection(DataSet &ds, unsigned int offset, 
		unsigned int n, vector<float> &data)
{
	//Empty vector before we refill
	data.clear();

	DataSpace slabSpace=ds.getSpace();

	//obtain the dimensions of this object
	unsigned int dimsFloat = slabSpace.getSimpleExtentNdims();
	
	if(dimsFloat !=1)
		return false;

	hsize_t dimSizes;
	slabSpace.getSimpleExtentDims(&dimSizes);


	//Check corner is in dataset
	if(offset*n >=dimSizes)
		return false;
	
	hsize_t start=offset*n;

	//Check and then shrink if the data is to small
	if(offset*n + n >=dimSizes)
	{
		//Ensure left most element is in range
		assert(offset*n < dimSizes);
		n=dimSizes-offset*n;
	}

	//Set count,and offset
	hsize_t count;
	count=n; 
	slabSpace.selectHyperslab(H5S_SELECT_SET,&count,&start);


	hsize_t nTmp=n;
	DataSpace memSpace(1,&nTmp);


	data.resize(n);

	ds.read(&(data[0]),PredType::NATIVE_FLOAT,
				memSpace,slabSpace);

	return true;

}

bool fetch2DSelection(DataSet &ds, unsigned int offsetRow,
		unsigned int nRows, vector<vector<float> > &data)
{
	//Empty vector before we refill
	data.clear();

	DataSpace slabSpace=ds.getSpace();

	//obtain the dimensions of this object
	unsigned int dimsFloat = slabSpace.getSimpleExtentNdims();
	if(dimsFloat !=2)
		return false;

	hsize_t dims[2];
	slabSpace.getSimpleExtentDims(dims);


	if(offsetRow > dims[0])
		return false;


	if(offsetRow + nRows > dims[0])
		nRows = dims[0] - offsetRow;

	data.resize(nRows);

	hsize_t count[2],start[2];
	count[0] =1;
	count[1]=dims[1];
	start[0]=offsetRow;
	start[1]=0;


	for(auto &v : data)
	{
		DataSpace memSpace(1,&dims[1]);

		v.resize(dims[1]);
		slabSpace.selectHyperslab(H5S_SELECT_SET,count,start);
	
		//read row
		ds.read(&(v[0]),PredType::NATIVE_FLOAT,
					memSpace,slabSpace);
	
		//increment start
		start[0]++;
	}

	return true;
}

unsigned int validateAPTHDF5(const string &filename, bool strict, bool verbose)
{

	if(!H5File::isHdf5(filename.c_str()))
		return APTH5_ERR_OPEN;



	const vector<pair <string,hid_t> >  requiredFields={	
						{"/ExperimentContext/SampleDescription", H5T_STRING} ,
						{"/ExperimentContext/ExperimentType", H5T_STRING},
						{"/ExperimentContext/SampleName", H5T_STRING},
						{"/ExperimentContext/SampleUniqueIdentifier", H5T_STRING},
						{"/ExperimentContext/ApertureType", H5T_STRING},
						{"/ExperimentContext/ApertureUniqueIdentifier", H5T_STRING},
						{"/ExperimentContext/Version", H5T_STRING}, 
						{"/ToolEnvironment/ExperimentStartDateUTC", H5T_STRING},
						{"/ToolEnvironment/ExperimentEndDateUTC", H5T_STRING},
						{"/ToolEnvironment/ExperimentStartDateLocal", H5T_STRING},
						{"/ToolStateAndSettings/DetectorGeometryOpticalEquiv", H5T_FLOAT}, 
						{"/ToolStateAndSettings/DetectorType", H5T_STRING},
						{"/ToolStateAndSettings/DetectorSize", H5T_FLOAT},
						{"/ToolStateAndSettings/DetectorReadout", H5T_STRING},
						{"/ToolStateAndSettings/DetectorResolution", H5T_FLOAT}, 
						{"/ToolStateAndSettings/FlightPathSpatial", H5T_FLOAT},
						{"/ToolStateAndSettings/FlightPathTiming", H5T_FLOAT},
						{"/ToolStateAndSettings/InstrumentIdentifier", H5T_STRING},
						{"/ToolStateAndSettings/LaserIncidence", H5T_FLOAT},
						{"/ToolStateAndSettings/LaserWavelength", H5T_FLOAT},
						{"/ToolStateAndSettings/ReflectronInfo", H5T_STRING},
						{"/ToolStateAndSettings/Tip2ReconSpaceMapping", H5T_FLOAT},
						{"/ExperimentResults/LaserPosition", H5T_FLOAT},
						{"/ExperimentResults/LaserEnergy", H5T_FLOAT},
						{"/ExperimentResults/PulseFrequency", H5T_FLOAT},
						{"/ExperimentResults/PulseFraction", H5T_FLOAT},
						{"/ExperimentResults/StandingVoltage", H5T_FLOAT},
						{"/ExperimentResults/ReflectronVoltage", H5T_FLOAT},
						{"/ExperimentResults/TipTemperature", H5T_FLOAT},
						{"/ExperimentResults/StagePosition", H5T_FLOAT},
						{"/ExperimentResults/TimeOfFlight", H5T_FLOAT},
						{"/ExperimentResults/DetectorHitPositions", H5T_FLOAT},
						{"/ExperimentResults/PulseNumber", H5T_INTEGER},
						};
	#ifdef DEBUG
		//FIXME: Code should be re-written to use map.
		// - enforce unique keys
		for(auto ui=0u;ui<requiredFields.size();ui++)
		{
			for(auto uj=ui+1;uj<requiredFields.size();uj++)
			{
				assert(!(requiredFields[ui].first ==requiredFields[uj].first));
			}
		}
	#endif

	//First entry is whether found, second is type
	vector<bool> foundField(requiredFields.size(),false);
	vector<hid_t> foundFieldType(requiredFields.size(),-1);

	try
	{
		//Exception::dontPrint();

		//Open the file.
		H5File file(filename.c_str(),H5F_ACC_RDONLY);
		hid_t fileID = file.getId();

		//Open the base "Group".
		//hid_t objID =//file.getObjId("/");
		auto baseGroup = file.openGroup("/");

		//Queue up the objects in this group
		deque<pair<HDFQueueEntry,string> > objQueue;
		for(auto ui=0u;ui<baseGroup.getNumObjs();ui++)
			objQueue.push_back(make_pair(HDFQueueEntry(fileID,ui,0),"/"));
		char *buffer = new char[BUF_MAX]; //FIXME: Use scoped pointer, for exception saftey

		//Traverse the directory heirarchy, 
		// - enumerate objects to console
		// - mark off required objects
		vector<string> groupStack;
		while(objQueue.size())
		{
			HDFQueueEntry entry = objQueue.front().first;
			string objPath=objQueue.front().second;
			objQueue.pop_front();

			//FIXME: Apparently this has been deprecated. However, the replacement
			//functon H5L_GET_NAME_BY_IDX requires a name, which we don't have
			ssize_t size;
			size=H5Gget_objname_by_idx(entry.parent,
					entry.offset,buffer,BUF_MAX) ; 


			//Get the object type
			int type;
			type=H5Gget_objtype_by_idx(entry.parent,entry.offset);
					
			//Print object tree entry
			string tabStr("\t",entry.depth);
			if(size>0)	
			{

				if(type >=HDF_TYPE_ENUM_END)
					return false;
				assert(type <HDF_TYPE_ENUM_END);


				if(verbose)
				{
					cerr << tabStr << "Obj : \"" << buffer << "\"";
					cerr << tabStr << " ObjType : " << HDF_TYPE_BYID[type] << endl;
				}
			} 
			else
			{
				if(strict)
					return APTH5_ERR_EMPTY_OBJECT;
			}


			string fullPath;
			fullPath=objPath + buffer;

			//See if it is in the required list, and mark in foundField
			size_t offset=(size_t)-1;
			for(auto ui=0u;ui<requiredFields.size(); ui++)
			{
				if(fullPath == requiredFields[ui].first)
				{
					foundField[ui]=true;
					offset=ui;
					break;
				}
			}

			//If it is a group (analagous to a directory), expand it
			switch(type)
			{
				case HDF_TYPE_GROUP:
				{
					//Open group
					hid_t locId = H5Gopen(entry.parent,buffer,H5P_DEFAULT);
		
					//Queue up the objects in the group.	
					hsize_t numObjects;
					if(H5Gget_num_objs(locId,&numObjects)>=0)
					{
						//Push each new object found on, recording the queue entry, and the full path
						for(auto ui=0u;ui<numObjects;ui++)	
							objQueue.push_front( make_pair(HDFQueueEntry(locId,ui,entry.depth+1),
										objPath + string(buffer) + string("/")) );
					}

					break;
				}
				case HDF_TYPE_DATASET:
				{
					tabStr+="\t";
					//herr_t H5LTread_dataset ( hid_t loc_id, const char *dset_name, hid_t type_id, void *buffer )
					hsize_t dims;
					H5T_class_t classId;
					size_t typeSize;
					if(H5LTget_dataset_info(entry.parent,buffer,
							&dims,&classId,&typeSize) < 0)
					{
						cerr << tabStr << "Unable to get dataset info" << endl;
					}

					hid_t datasetId = H5Dopen(entry.parent,buffer,H5P_DEFAULT);
					if(datasetId <0)
					{
						cerr << tabStr<< "unable to open dataset" << endl;
						break;
					}
					DataSet dataSet(datasetId);
					H5T_class_t classType =dataSet.getTypeClass();

					if(offset !=(size_t)-1)
						foundFieldType[offset]=classType;
					auto itType=hdf5TypeMap.find(classType);
					
					if(verbose)
					{
						if(itType!=hdf5TypeMap.end())
						{
							cerr << tabStr << "type :" << itType->second << endl;
						}
						else 
							cerr << tabStr << "type: unknown" << endl;
					}


					const unsigned int MAX_DISPLAY=20;
					switch(classType)
					{
						case H5T_STRING:
						{
							std::string s;
							DataSpace dSpace = dataSet.getSpace();
							auto dType=dataSet.getDataType();
							dataSet.read(s,dType);

							if(verbose)
								cerr << tabStr << "Contents : " << s << endl;
							break;
						}
						case H5T_FLOAT:
						{
							float *f;
							
							DataSpace dSpace = dataSet.getSpace();
							unsigned int nPoints = dSpace.getSimpleExtentNpoints();
							f = new float[nPoints];
							auto dType=dataSet.getDataType();
							//Shouldn't I be using NATIVE_FLOAT?
							dataSet.read(f,PredType::NATIVE_FLOAT);

							//Obtain dimensions
							unsigned int dimsFloat = dSpace.getSimpleExtentNdims();
							hsize_t *dimSizes = new hsize_t[dimsFloat];
							dSpace.getSimpleExtentDims(dimSizes);

							//Print out dimension
							if(verbose)
							{
								cerr << tabStr << "Dimensions: ";
								for(auto ui=0u;ui<dimsFloat;ui++)
								{
									cerr << dimSizes[ui];
									if(ui+1<dims)
										cerr << "x";
								}
								cerr << endl;

						


								unsigned int nDisplay = std::min(nPoints,MAX_DISPLAY);
								cerr << tabStr << " = Sample Contents =" << endl;
								for(auto ui=0u; ui<nDisplay;ui++)
									cerr << tabStr << ui << "|" << f[ui] << endl;
								cerr << tabStr << " ========== =" << endl;
							}

							//FIXME: Load in chunks, rather than all at once
							if(dimsFloat ==1) 
							{
								const unsigned int CHUNK_SIZE=100000;
								auto nItems=dataSet.getSpace().getSimpleExtentNpoints();
								auto nFetches = nItems/CHUNK_SIZE;


								vector<float> data;

								for(auto ui=0;ui<nFetches;ui++)
								{
									if(!fetch1DSelection(dataSet,ui,CHUNK_SIZE,data))
									{
										cerr << "Failed to fetch data." << fullPath << endl;
										return false;
									}

									//check data is OK - should not contain nan
									for (auto &v : data)
									{
										if(std::isnan(v) || std::isinf(v))
										{
											cerr << "Nan or inf found in :" <<  fullPath;
											return APTH5_ERR_NONPHYSICAL_ENTRY;
										}
									}
								}
							}


							delete[] f;
							delete[] dimSizes;
							break;

						}
						case H5T_INTEGER:
						{
							uint64_t *data;
							
							DataSpace dSpace = dataSet.getSpace();
							unsigned int nPoints = dSpace.getSimpleExtentNpoints();
							data = new uint64_t[nPoints];
							auto dType=dataSet.getDataType();
							dataSet.read(data,PredType::NATIVE_UINT64);

							//Obtain dimensions
							unsigned int dimsInt = dSpace.getSimpleExtentNdims();
							hsize_t *dimSizes = new hsize_t[dimsInt];
							dSpace.getSimpleExtentDims(dimSizes);

							if(verbose)
							{
								//Print out dimension
								cerr << tabStr << "Dimensions: ";
								for(auto ui=0u;ui<dimsInt;ui++)
								{
									cerr << dimSizes[ui];
									if(ui+1<dimsInt)
										cerr << "x";
								}
								cerr << endl;

								unsigned int nDisplay = std::min(nPoints,MAX_DISPLAY);
								cerr << tabStr << " = Sample Contents =" << endl;
								for(auto ui=0u; ui<nDisplay;ui++)
									cerr << tabStr << ui << "|" << data[ui] << endl;
								cerr << tabStr << " ========== =" << endl;
							}

							delete[] data;
							delete[] dimSizes;
							break;

						}
						default:
						{
							if(strict)
								return APTH5_ERR_UNKNOWN_OBJECT;
							cerr << "BUG: Data type not handled yet" <<endl;
						}

					}

					

				}
				default:
					//Do-nothing
					;
			}
		}

		delete[] buffer;

	
		vector<string> sameSizeFields = { 
							"/ExperimentResults/PulseNumber",
							"/ExperimentResults/TimeOfFlight", 
						};
		

		string reflectType;
		//If reflectrontype is not "None", then we should have same size field
		{
			H5::Exception::dontPrint() ; //silence exception which will occur if we don't have this entry	
			try
			{
				const char *REFLECT_TYPE_STR="/ExperimentResults/ReflectronInfo";
				DataSet dataSet = file.openDataSet( REFLECT_TYPE_STR);
				DataSpace dSpace = dataSet.getSpace();
				dataSet.read(reflectType,dataSet.getDataType());
			}
			catch(...)
			{

			}
			//Restore exception printing behaviour
			int(*f)(long int,void*);
			f = (int(*)(long int, void*))H5Eprint;
			H5::Exception::setAutoPrint(f,stderr);
		}


		//Check each of the sameSizeFields actually has a matching entry
		bool haveAllSameSizeFields=true;
		for(const auto &s : sameSizeFields)
		{
			for(auto ui=0u;ui<foundField.size();ui++)
			{
				if(requiredFields[ui].first == s)
					continue;
				if( foundField[ui] == (unsigned int) -1)
				{
					haveAllSameSizeFields=false;
					cerr << "Required field not found, and is a field that should be of the same size as other fields" << endl;
					cerr << "\t" << s<< endl;
					break;
				}

			}
		}

		//Abort before attempting to validate field sizes
		if(!haveAllSameSizeFields)
			return APTH5_ERR_MISSING_FIELDS;

		//validated that the field sizes match - note that this assumes the fields exist
		unsigned int mismatch;
		if(!validateFieldSizesMatch(file,sameSizeFields,mismatch))
		{
			if(mismatch !=(unsigned int)-1)
				cerr << "Mismatched field size :" << sameSizeFields[mismatch] << endl;
			else
				cerr << "Unknown problem when validating field sizes." << endl;

			return APTH5_ERR_UNKNOWN_EXCEPTION;
		}

		//If we are performing strict  validation, check type data
		if(strict && std::find(foundField.begin(),
			foundField.end(),false) !=foundField.end())
		{
			bool errorFound=false;
			for(auto ui=0u;ui<foundField.size();ui++)
			{
				if(!foundField[ui])
				{
					errorFound=true;
					cerr << "Missing Required:" << requiredFields[ui].first << endl;
				}
				else
				{
					assert(foundFieldType[ui] != -1);
					if(foundFieldType[ui] !=requiredFields[ui].second)
					{
						errorFound=true;
						string foundTypeName;
						if(hdf5TypeMap.find(foundFieldType[ui]) != hdf5TypeMap.end() )
							foundTypeName=hdf5TypeMap.at(foundFieldType[ui]);
						else
							foundTypeName="Unknown type";

						cerr << "Type Mismatch :" << requiredFields[ui].first << 
							" should be of type :" << hdf5TypeMap.at(requiredFields[ui].second) << " but is " << foundTypeName << endl;
					}
				}
			}

			if(errorFound)
				return APTH5_ERR_MISSING_FIELDS;
		}
	

		if(!validateMetaData(file))
			return APTH5_ERR_NONPHYSICAL_ENTRY;

		if(!validatePhysicalParameters(file))
			return APTH5_ERR_NONPHYSICAL_ENTRY;


	}
	catch(std::exception &e)
	{
		cerr << "Exception when using/opening HDF file" << endl;
		return APTH5_ERR_UNKNOWN_EXCEPTION;
	}


	return 0;
}

bool validateFieldSizesMatch(H5File &file, const vector<string> &paths, unsigned int &firstMismatch)
{
	if(!paths.size())
		return true;

	firstMismatch=(unsigned int)-1;

	unsigned int dataSize;
	//Obtain the dimensions of the first entry
	{
	DataSet ds;
	ds=file.openDataSet(paths[0].c_str());

	DataSpace dSpace;
	dSpace=ds.getSpace();

	dataSize=dSpace.getSimpleExtentNpoints();
	}

	for(auto ui=1u;ui<paths.size();ui++)
	{
		DataSet ds;
		ds=file.openDataSet(paths[ui].c_str());

		DataSpace dSpace;
		dSpace=ds.getSpace();

		unsigned int tmpSize;
		tmpSize=dSpace.getSimpleExtentNpoints();

		if(tmpSize !=dataSize)
		{
			cerr << "Mismatched field :" << paths[ui] << " : " << tmpSize << endl;
			firstMismatch=ui;
		}
	}

	if(firstMismatch !=(unsigned int)-1)
		return false;

	return true;

}

void writeDataToGroup(H5File &file, 
		const string &datasetName, const vector<string> &dataVec)
{
	//This is a little silly, we have to "copy" the type, and change its size for every string.
	// Not sure if its possible to store strings of different sizes
	hsize_t dims=1;
	hid_t strType=H5Tcopy(H5T_C_S1);
	H5Tset_size (strType, H5T_VARIABLE);
	DataSpace dataSpace(1,&dims);
	DataSet *dSampleDescription = new DataSet(file.createDataSet(datasetName.c_str(),strType,dataSpace));
	const char *data[dataVec.size()];
	for(auto ui=0u;ui<dataVec.size();ui++)
		data[ui]=dataVec[ui].c_str();
	H5Dwrite(dSampleDescription->getId(), strType, dataSpace.getId(), H5S_ALL, H5P_DEFAULT, data);

	delete dSampleDescription;
}

void writeDataToGroup(H5File &file, const string &datasetName, const vector<float> &data,
			bool compression)
{
	hsize_t dSize=data.size();
	DataSpace dataSpace(1,&dSize);
	DataSet *dSampleDescription;
	
	if(!compression)
		dSampleDescription= new DataSet(file.createDataSet(datasetName.c_str(),PredType::NATIVE_FLOAT,dataSpace));
	else
	{
		//Set up the compression settings
		hsize_t chunkSize = 10000;
		chunkSize=std::min(chunkSize,(hsize_t)data.size());

		DSetCreatPropList pList;
		pList.setChunk( 1, &chunkSize);
		pList.setDeflate( 6 );

		dSampleDescription= new DataSet(file.createDataSet(datasetName.c_str(),PredType::NATIVE_FLOAT,dataSpace,pList));

	}

	H5Dwrite(dSampleDescription->getId(), H5T_NATIVE_FLOAT, 
			dataSpace.getId(), H5S_ALL, H5P_DEFAULT, &(data)[0]);
	
}

void writeDataToGroup(H5File &file, const string &datasetName, const vector<uint64_t> &data,
			bool compression)
{
	hsize_t dSize=data.size();
	DataSpace dataSpace(1,&dSize);
	DataSet *dSampleDescription;
	
	if(!compression)
		dSampleDescription= new DataSet(file.createDataSet(datasetName.c_str(),PredType::NATIVE_UINT64,dataSpace));
	else
	{
		//Set up the compression settings
		hsize_t chunkSize = 10000;
		chunkSize=std::min(chunkSize,(hsize_t)data.size());

		DSetCreatPropList pList;
		pList.setChunk( 1, &chunkSize);
		pList.setDeflate( 6 );

		dSampleDescription= new DataSet(file.createDataSet(datasetName.c_str(),PredType::NATIVE_UINT64,dataSpace,pList));

	}

	H5Dwrite(dSampleDescription->getId(), H5T_NATIVE_FLOAT, 
			dataSpace.getId(), H5S_ALL, H5P_DEFAULT, &(data)[0]);
	
}

void writeDataToGroup(H5File &file, const string &datasetName, const float* data, 
			unsigned int rows, unsigned int cols, bool compression)
{
	hsize_t dSize[] ={ 
		rows,cols };
	DataSpace dataSpace(2,dSize);

	
	DataSet *dSampleDescription ;
	if(!compression)
		dSampleDescription= new DataSet(file.createDataSet(datasetName.c_str(),PredType::NATIVE_FLOAT,dataSpace));
	else
	{
		//Set up the compression settings
		hsize_t chunkSize[2] = {
			100,100};
		chunkSize[0]=std::min(chunkSize[0],(hsize_t)rows);
		chunkSize[1]=std::min(chunkSize[1],(hsize_t)cols);

		DSetCreatPropList pList;
		pList.setChunk( 2, chunkSize); 
		pList.setDeflate( 6 );

		dSampleDescription= new DataSet(file.createDataSet(datasetName.c_str(),PredType::NATIVE_FLOAT,dataSpace,pList));

	}

	H5Dwrite(dSampleDescription->getId(), H5T_NATIVE_FLOAT, 
			dataSpace.getId(), H5S_ALL, H5P_DEFAULT, data);
	
}

#ifdef DEBUG

bool runCommonTests()
{
	//Run ISO-8601 timepoint tests
	//Basic, minute precision:
	vector<string> timePoints= {	"19850412T1015",		//Minute precision, basic
					"1985-04-12T10:15",		//Minute precision, extended
					"19850412T101530",		//Second precision, basic
					"1985-04-12T10:15:30", 		//Second precision, extended
					//"1985102T1015Z",		// Minute precision, basic, UTC (ordinal)
					//"1985-102T10:15Z",		//Minute precision, extended, UTC (ordinal)
					"19850412T101530Z", 		//second precision, basic, UTC
					"1985-04-12T10:15:30Z",		//second precision, extended, UTC
					"19850412T101530+0400", 	//second precision, timezone
					"19850412T101530+04", 		//second precision, timezone (2)
					
					"1985-04-12T10:15:30+04:00",	// With timezone, extended
					"1985-04-12T10:15:30+04",	//With timezone, extended (2)
					"20200512T183848Z", //User submitted, matlab
					//"20200512T193848+01:00", // User submitted, matlab with TZ This mixes extended and basic, which is not allowed, I think

					};

	cerr << "Checking ISO8601..." ;
	unsigned int failCount=0;
	for(auto s : timePoints)
	{
		if(!isISO8601(s))
		{
			failCount++;
			cerr << "Fail" <<  s << endl;
		}
	}
	if(failCount)
		cerr << "failed :" << failCount << " of " << timePoints.size() << "example" << endl;
	else
		cerr << "OK" << endl;
	cerr << endl;

	return true;
}

#endif

