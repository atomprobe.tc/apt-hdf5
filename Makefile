CC=gcc
CFLAGS=-Wall 
all: CFLAGS+= 
debug: CFLAGS+= -g -DDEBUG 
optimal: CFLAGS+= -O3 -mfpmath=sse 

INCLUDES=-I/usr/include/hdf5/serial 
LINKFLAG=`h5cc -show -shlib | sed 's/.*-L/-L/' | sed 's/ -.*//'` -lhdf5_cpp -lhdf5_serial -lhdf5_serial_hl -lm -lstdc++
TARGET=hdftest
OBJECTS=common.o

all: $(TARGET)

debug: $(TARGET)

install: $(TARGET)
	cp $(TARGET) /usr/local/bin/


$(TARGET): $(TARGET).cpp $(OBJECTS)
	$(CC) $(TARGET).cpp $(OBJECTS) $(CFLAGS) $(INCLUDES) $(LINKFLAG) -o $(TARGET)

%.o: %.cpp %.h
	$(CC) -c $(CFLAGS) $(INCLUDES) -o $@ $<

clean:
	-rm -f *.o
	-rm -f $(TARGET)


